(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('PagoController', PagoController);
    PagoController.$inject = ['$rootScope', '$location', 'dataservice'];

    function PagoController($rootScope, $location, dataservice) {
        var vm = this;
        vm.vista = $rootScope.vista ? $rootScope.vista : "pagar"; //"pagar";
//        vm.vista =  "pago"; //"pagar";
        vm.ResumenViaje = "poiuy";
        vm.idChofer = "";
        vm.accestchofer = "";// este vuela
        vm.campaign_id = false;// para componer el post de pago con las datos del cupon
        vm.coupon_code = false;
        vm.coupon_amount = false;

        function _init(){
           console.log("_init: " + $rootScope.vista + " / " + vm.vista);
            console.log("dataservice.paymentctual': " + dataservice.paymentctual);
           if(vm.vista == 'pago')
           {
               console.log("_init vista pago A': " + $rootScope.vista + " / " + vm.vista);
               //vm.vista == 'pagar';
               console.log("_init vista pago B': " + $rootScope.vista + " / " + vm.vista);
               return;
           } 
           
           if(dataservice.paymentctual != "No aplicativo"){
                return;
           }
            
            dataservice.searchCustomer({email: dataservice.user['E-Mail']}).then(function(res){
                if (res.data.status == 200){
                    var user = res.data.response;
                    if (user.paging.total == 1){
                        var result = user.results[0];
                        if (result.cards.length){
                            console.log("TIENE TARJETA ");
                            vm.tarjetas = result.cards;
                           // console.log("TIENE TARJETA " + vm.tarjetas.length);                            
                            for (var i = 0; i< vm.tarjetas.length;i++){
                                vm.tarjetas[i]["label"] = vm.tarjetas[i].last_four_digits + "-" + vm.tarjetas[i].issuer.name;
                               // console.log("  vm.tarjetas: " + JSON.stringify(vm.tarjetas[i])); 
                            }
//                            vm.tarjetas.push(["label"] = "Pagar com outro cartão de crédito");
//                            vm.tarjetas.push(vm.tarjetas[0]);
//                            vm.tarjetas[vm.tarjetas.length]["label"] = "Pagar com outro cartão de crédito";

                            //console.log("  vm.tarjetas: " + JSON.stringify(vm.tarjetas));                            
                            vm.tarjeta = vm.tarjetas[0];
                            return;
                        }
                        console.log("NO TIENE TARJETA");
                        $location.url('/home/tcreditosdata');
                        return;
                    }
                    $location.url('/home/tcreditosdata');
                }
            });
        }

        vm.pagar = function () {
            //_getPaymentMethod();
            //return;
            // 1 configuro las credeciales del chofer
            //dataservice.setCredenChofer(vm.idChofer)
            console.log("  en click " + vm.idChofer + " " + dataservice.user.Nombre + " " + dataservice.user.Apellido);
           // _getCupon();
            
            // dataservice.setCredenChofer("446")
            dataservice.setCredenChofer(vm.idChofer)
            .then(function (datacreden) {
                var respuesta = {
                    "MPaccesstoken": datacreden.data.accesstoken,
                    "MPPublickey": datacreden.data.publickey,
                    "MPrefreshtoken": datacreden.data.refreshtoken
                };
                console.log("  respuesta nuevas publickey chofer: " + datacreden.data.publickey);
                vm.accestchofe = datacreden.data.accesstoken;                
                Mercadopago.setPublishableKey(datacreden.data.publickey);// seteo la publickey del chofer
                Mercadopago.getIdentificationTypes();
                
                //*/*/*/*/*/
                _getPaymentMethod();
                //return;
                ///*/*/*/*/*/*/

                //*/*/*/*/*/
               // _getCupon();
                ///*/*/*/*/*/*/
            })
        };

        function _getCupon () {
            //_getPaymentMethod();
            //return;
            // 1� configuro las credeciales del chofer

            console.log("  en getcupon ");
           // dataservice.getCupon(vm.idChofer)
            var valor = vm.ValorPagado; 
            valor = valor.replace(",", ".");
            dataservice.getCupon(vm.idChofer, valor, dataservice.user["E-Mail"])
           // dataservice.getCupon("446", 30, dataservice.user["E-Mail"])
            .then(function (datacupon) {
                console.log("  respuesta CUPON: " + JSON.stringify(datacupon.data));

                vm.campaign_id = datacupon.data.response.id;
                vm.coupon_code = "2DOTESTE";
                vm.coupon_amount = datacupon.data.response.coupon_amount;

                console.log("  vm.campaign_id: " + vm.campaign_id);
                console.log("  vm.coupon_code: " + vm.coupon_code);
                console.log("  vm.coupon_amount: " + vm.coupon_amount);

                //return;
                //*/*/*/*/*/
                _getPaymentMethod();
                ///*/*/*/*/*/*/
            })
        };


        function _getPaymentMethod() {
            console.log("  _getPaymentMethod");
        //2� obetengo los metodos de pago del pasajero 
            Mercadopago.getPaymentMethod({
                "bin": vm.tarjeta.first_six_digits
            }, _setPaymentMethodInfo);            
        };


        function _setPaymentMethodInfo(status, data) {
            console.log("  _setPaymentMethodInfo");

        //3� seteo el metodo de pago del pasajero 
            if (status == 200){
                var $form = document.querySelector('#pay');
                dataservice.payment.info = data;
                Mercadopago.createToken($form, _getToken);
            }
        }

        function _getToken(status, data) {
            console.log("  _getToken");
            console.log("      data con token: " + JSON.stringify(data));
            console.log("      dataservice.payment.info: " + JSON.stringify(dataservice.payment.info));

            var valor = vm.ValorPagado; 
            valor = valor.replace(",", ".");
            var valor_fee = valor * 0.0499;
            valor_fee = parseFloat(valor_fee).toFixed(2);
           // console.log("valor_fee con 2 dec OK?: " + parseFloat(valor_fee));
          //  console.log("valor_fee: " + parseFloat(valor_fee));
          //  console.log("valor_fee 2 dec: " + parseFloat(valor_fee).toFixed(2));

        //5� compongo el objto para enviar a user.paymentchofer/....vi/payment
            var payment = {
                amount: parseFloat(valor),
                token: data.id,
                description: "Vigem de taxi feito",
                installments: 1,
                method_id: dataservice.payment.info[0].id,
                customer_id: vm.tarjeta.customer_id,
                email: dataservice.user["E-Mail"],
                fee: parseFloat(valor_fee),
                idChofer: vm.idChofer, // 23062016
                idAVL: vm.id_equipo, // 02092016,
                Nombre: dataservice.user.Nombre,
                Apellido: dataservice.user.Apellido
            };
            if (vm.campaign_id) {//06072016
                console.log("    *****payment con cupon***** " ); 
                payment = {
                    amount: parseFloat(valor),
                    token: data.id,
                    description: "Vigem de taxi feito",
                    installments: 1,
                    method_id: dataservice.payment.info[0].id,
                    customer_id: vm.tarjeta.customer_id,
                    email: dataservice.user["E-Mail"],
                    fee: parseFloat(valor_fee),
                    idChofer: vm.idChofer,
                    idAVL: vm.id_equipo, // 02092016,                    
                    coupon_code: vm.coupon_code,//06072016
                    coupon_amount: vm.coupon_amount,
                    Nombre: dataservice.user.Nombre,
                    Apellido: dataservice.user.Apellido
                };
             };

            console.log(" card token: " + data.id);

            //return; //23062016
            var error = function (data) {
               
                vm.vista = "error";
                console.log("      ERROR /*/* *** data.response" + JSON.stringify(data));
                var datamsj = {
                    idChofer: vm.idChofer,
                    payment: data.message // data.data.response -> ver que mesj envio al chofer para tratar el error del pago
                }
                dataservice.pagoMsjChofer(datamsj)

            };

        //6 voy a dataservice pra procesar el pago
            dataservice.doPayment(payment,  dataservice.totalappFeeAcobrar)
                .then(function (data) {
                    if (data.data.status == 400) {
 //                  if (true) {
                        error(data);

                  //      var datamsj = {
                  //          idChofer: vm.idChofer,
                  //          payment: data.data.response
                  //      }
                  //      dataservice.pagoMsjChofer(datamsj);
                        //.then(function (data) { }) // habilitar cuando este definido la mensajeria final
                   //     vm.vista = "error";// 08092016
                        //$rootScope.vista = "error";// 08092016
                        //$location.url('/home/pagaviaje');// 08092016
                        return;
                    }
                    //else{
                        dataservice.payment.purchate = data.data.response;
                        console.log(" -----**** data.response: " + JSON.stringify(data.data.response));
                        vm.vista = "pago";
                    //    $rootScope.vista = "pago";// 08092016
                    //    $location.url('/home/pagaviaje');// 08092016
                        dataservice.neteoviaje(data.data.response.id)
                        .then(function (dataneteoviaje) {
                            console.log(" ");
                            console.log(" ");
                            console.log(" ");
                            console.log(" data.data.response.id: " + data.data.response.id);
                        })

                        var datamsj = {
                            idChofer: vm.idChofer,
                            payment: data.data.response
                        }

                        dataservice.pagoMsjChofer(datamsj)
                        .then(function (data) {
                            return;
                        })
                    //}
                })
                .catch(error);
        }
        _init();
    }
})();
