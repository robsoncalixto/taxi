(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('TarjetaController', TarjetaController);

    TarjetaController.$inject = ['$rootScope', '$location', 'dataservice'];

    function TarjetaController($rootScope, $location, dataservice) {
        var mp = this;
            mp.tarjeta = {
                email: dataservice.user['E-Mail'],//'alexandre.borges@autotaximetros.com.br',
                nombre: dataservice.user.Nombre + " " + dataservice.user.Apellido,// 'alexandre b araujo',
                documentoTipo: 'CPF',
                documentoNumero: dataservice.user.DNI,// 01635975700,//'',
                numero: '',// '5031433215406351',// 4093085655876846,//'',
                codigo: '',//"767",//'',
                mes: '',//12,//'',
                ano: '',//2017,//'',
            };
            mp.idChofer = "";
            mp.guardarme = true;
            mp.quetarjeta = '';
            mp.idcustomer = '';
            mp.cardtoken = '';
            mp.ValorViaje = '';
            mp.meses = ["01","02","03","04","05","06","07","08","09","10","11","12"];
            mp.anios = ["2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030"];
            mp.logoTC = false;    
            mp.ExisteCustom = false;   
            
        var vm = this;
        vm.idChofer = "";
        vm.accestchofer = "";// este vuela
        vm.campaign_id = false;// para componer el post de pago con las datos del cupon
        vm.coupon_code = false;
        vm.coupon_amount = false;


        mp.searchCustomermp = function () {
            dataservice.searchCustomer(mp.tarjeta)
            .then(function (searchcustom) {
                console.log("  respuesta: " + JSON.stringify(searchcustom));
            })    
        };

        mp.crearCustomermp = function () {
            dataservice.crearCustomer(mp.tarjeta)
            .then(function (crearcustom) {
                console.log("  respuesta: " + JSON.stringify(crearcustom));                
            })            
        };

        mp.addcardmp = function () {
            dataservice.addcard(mp.tarjeta)
            .then(function (retornoadddcard) {
                console.log("  respuesta: " + JSON.stringify(retornoadddcard));
            })
        };

        mp.getcardmp = function () {
            console.log("  respuesta: " + JSON.stringify(mp.tarjeta));
            dataservice.getcard(mp.tarjeta)
            .then(function (getcard) {
                console.log("  respuesta: " + JSON.stringify(getcard));
            })
        };


        mp.removecustomermp = function () {
            console.log("removecustomermp ");
            dataservice.removecustomer(mp.tarjeta)
            .then(function (retornoadddcard) {
                console.log("  respuesta: " + JSON.stringify(retornoadddcard));
            });
        };

        
        mp.updatecustomp = function () {
            var $form = document.querySelector('#pay');
            dataservice.updatecusto($form)
            .then(function (updatecusto) {
                console.log("  respuesta: " + JSON.stringify(updatecusto));
            });          
        };

        mp.delcardcustomermp = function () {
            dataservice.delcardcustomer(mp.tarjeta)
            .then(function (delcardcustomer) {
                console.log("  respuesta: " + JSON.stringify(delcardcustomer));
            });
        };
        
        mp.updatecardcustomp = function () {
            dataservice.updatecardcusto()
            .then(function (updatecardcusto) {
                console.log("  respuesta: " + JSON.stringify(updatecardcusto));
            })            ;
        };


        mp.getbandera = function () {
            console.log("primeros6) ", mp.tarjeta.numero.toString().substring(0, 6));
            dataservice.getbandera(mp.tarjeta.numero.toString().substring(0, 6))
            .then(function (getbandera) {
//                mp.quetarjeta = getbandera.data.issuer.name;
                mp.quetarjeta = getbandera.data.payment_method_id;
                mp.logoTC = getbandera.data.issuer.secure_thumbnail;
                console.log("  respuesta: " + mp.quetarjeta + mp.logoTC);
                //console.log("  respuesta: " + JSON.stringify(getbandera));
            });
            
            dataservice.searchCustomer(mp.tarjeta)
            .then(function (searchcustom) {
                //console.log("  respuesta: " + JSON.stringify(searchcustom.data.response.results[0]));
                mp.ExisteCustom = false;// solo para boludear con el click
                console.log("existe el cliente A: " + mp.ExisteCustom);
                try {
                    if(searchcustom.data.response.results[0].id){
                        mp.ExisteCustom = true;
                    }
                } catch (ex) {}
                console.log("existe el cliente B: " + mp.ExisteCustom);
            })   
        };
        
        

        mp.pagarAnterior = function(){
            var primeros6 = mp.tarjeta.numero.toString();
            console.log("primeros6.substring(0, 6) ", primeros6.substring(0, 6));
            Mercadopago.getPaymentMethod({
//                "bin": primeros6.substring(0, 6)
                "bin": mp.tarjeta
            }, _setPaymentMethodInfoG);
        };



        function _setPaymentMethodInfoG(status, data){
            console.log("_setPaymentMethodInfoG ", status, data);
            if (status == 200){
                var $form = document.querySelector('#pay');
               // dataservice.payment.info = data;
                Mercadopago.createToken($form, _getTokenG);
            }
        }


        function _getTokenG(status, data){
            console.log("_getTokenG", status);
            console.log(' token = ',data);
            if (status == 200){
                dataservice.payment.token = data;
                pagarCompleto();
                /*
                //guardo la tarjeta para recUperarla y Pagar 
                dataservice.addcard(mp.tarjeta)
                .then(function (retornoadddcard) {
                    console.log("  respuesta: " + JSON.stringify(retornoadddcard));
                    dataservice.searchCustomer(mp.tarjeta)
                    .then(function (searchcustom) {
                        //console.log("  respuesta: " + JSON.stringify(searchcustom));
                        // console.log("  searchcustom.data: " + JSON.stringify(searchcustom.data));
                        console.log("  searchcustom.data.results: " + JSON.stringify(searchcustom.data.response.results));
                        mp.idcustomer = searchcustom.data.response.results[0].id;
                        console.log("  mp.idcustomer: " + mp.idcustomer);
                        //obtengo que tarjeta es
                        dataservice.getcard(mp.tarjeta)
                        .then(function (getcard) {
                            var conchudo = (getcard.data).toString();
                            var splitcard = conchudo.split(",");
                            var solocard = splitcard[0].split(" ");
                            var bandera = solocard[1];
                            mp.quetarjeta = bandera; // ojo si tiene mas de 1 tarjeta
                            console.log("  mp.quetarjeta: " + mp.quetarjeta);
                            pagarCompleto();
                        })      
                    })
                }) 

                */               
            }
        }


//

        //mp.pagarCompleto = function () {
//        function pagarCompleto(){
        mp.pagar = function(){                      
            //_getPaymentMethod();
            //return;
            // 1� configuro las credeciales del chofer
            //dataservice.setCredenChofer(vm.idChofer)

           console.log(" GUARDAR TCC " + mp.guardarme);


            console.log("  en click " + " " + dataservice.user.Nombre + " " + dataservice.user.Apellido);
            console.log("  en click " + vm.idChofer + " 2 "+ mp.idChofer + " "  + dataservice.user.Nombre + " " + dataservice.user.Apellido);
           // _getCupon();
            
            // dataservice.setCredenChofer("446")
            dataservice.setCredenChofer(vm.idChofer)
            .then(function (datacreden) {
                var respuesta = {
                    "MPaccesstoken": datacreden.data.accesstoken,
                    "MPPublickey": datacreden.data.publickey,
                    "MPrefreshtoken": datacreden.data.refreshtoken
                };
                console.log("  respuesta nuevas publickey chofer: " + datacreden.data.publickey);
                vm.accestchofe = datacreden.data.accesstoken;                
                Mercadopago.setPublishableKey(datacreden.data.publickey);// seteo la publickey del chofer
                Mercadopago.getIdentificationTypes();
                
                //*/*/*/*/*/
                _getPaymentMethod();
                //return;
                ///*/*/*/*/*/*/

                //*/*/*/*/*/
               // _getCupon();
                ///*/*/*/*/*/*/
            })
        };

//50314332

    function _getPaymentMethod() {
        var primeros6 = mp.tarjeta.numero.toString();
        console.log("primeros6.substring(0, 6) ", primeros6.substring(0, 6));
        Mercadopago.getPaymentMethod({
//                "bin": vm.tarjeta.first_six_digits            
            "bin": primeros6.substring(0, 6)
        }, _setPaymentMethodInfo);       

/*
        console.log("  _getPaymentMethod" +  mp.tarjeta.numero);
    //2 obetengo los metodos de pago del pasajero 
        Mercadopago.getPaymentMethod({
//                "bin": vm.tarjeta.first_six_digits
            //"bin": 503143
//                "bin": mp.tarjeta.numero
            "bin": mp.tarjeta
        }, _setPaymentMethodInfo);    
*/                
    };


        function _setPaymentMethodInfo(status, data) {
            console.log("  _setPaymentMethodInfo" + " status y data" + data);

        //3 seteo el metodo de pago del pasajero 
            if (status == 200){
                var $form = document.querySelector('#pay');
                dataservice.payment.info = data;
            console.log("      dataservice.payment.info: ---->" + JSON.stringify(data));                
                Mercadopago.createToken($form, _getToken);
            }
        }

        function _getToken(status, data) {
            console.log("  _getToken");

           // return;
        //4� obtengo el token de la tarjeta con las credenciales del chofer configuradas en 1�
            console.log("      dataservice.payment.info: " + JSON.stringify(dataservice.payment.info));
            console.log("      dataservice.totalappFeeAcobrar: " + JSON.stringify(dataservice.totalappFeeAcobrar));

            console.log("mp.ValorViaje : " + mp.ValorViaje);
            var valor = mp.ValorViaje.toString();// 3; //vm.ValorPagado; 
            valor = valor.replace(",", ".");
            //var valor_fee = valor * 0.0499;
            //valor_fee = parseFloat(valor_fee).toFixed(2);
            console.log("valor con 2 dec : " + parseFloat(valor));
          //  console.log("valor_fee: " + parseFloat(valor_fee));
          //  console.log("valor_fee 2 dec: " + parseFloat(valor_fee).toFixed(2));

        //5 compongo el objto para enviar a user.paymentchofer/....vi/payment
            var payment = {
                amount: parseFloat(valor),
                token: data.id,// dataservice.payment.token, //data.id,
                description: "Vigem de taxi feito",
                installments: 1,
                method_id: mp.quetarjeta,// 'master',// dataservice.payment.info.id,//dataservice.payment.info[0].id,
                customer_id: mp.idcustomer, //dataservice.user["E-Mail"],//'207949802-5sphn2bX0VuAqt',// vm.tarjeta.customer_id,
                email: dataservice.user["E-Mail"],
               // fee: parseFloat(valor_fee),
                idChofer: vm.idChofer, // 23062016
                idAVL: vm.id_equipo, // 02092016,
                Nombre: dataservice.user.Nombre,
                Apellido: dataservice.user.Apellido
            };
            if (vm.campaign_id) {//06072016
                console.log("    *****payment con cupon***** " ); 
                payment = {
                    amount: parseFloat(valor),
                    token: data.id,
                    description: "Vigem de taxi feito",
                    installments: 1,
                    method_id: mp.quetarjeta,
                    customer_id: mp.idcustomer,
                    email: dataservice.user["E-Mail"],
                   // fee: parseFloat(valor_fee),
                    idChofer: vm.idChofer,
                    idAVL: vm.id_equipo, // 02092016,                    
                    coupon_code: vm.coupon_code,//06072016
                    coupon_amount: vm.coupon_amount,
                    Nombre: dataservice.user.Nombre,
                    Apellido: dataservice.user.Apellido
                };
             };

            console.log(" card token: " + data.id);

            //return; //23062016
            var error = function (data) {
         //       console.log("      ERROR /*/* *** data.response" + JSON.stringify(payment));
                var datamsj = {
                    idChofer: vm.idChofer,
                    payment: 'data.message'
                }
                dataservice.pagoMsjChofer(datamsj);
                 $rootScope.vista = "error";
                 $location.url('/home/pagaviaje');                 

            };


        //6 voy a dataservice pra procesar el pago
            dataservice.doPayment(payment,  dataservice.totalappFeeAcobrar)
                .then(function (data) {
                    if (data.data.status == 400) {
                       console.log("   error 400" + JSON.stringify(data)); 
                        error(data);

                    //    var datamsj = {
                    //        idChofer: vm.idChofer,
                    //        payment: data.data.response
                    //    }
                    //    //dio error borro la tarjeta que guarde para poder pagar
                    //        dataservice.delcardcustomer(mp.tarjeta)
                    //        .then(function (delcardcustomer) {
                    //            console.log("  borro tarjeta: " + JSON.stringify(delcardcustomer));
                    //        });
                    //    //dio error borro la tarjeta que guarde para poder pagar

                    //    dataservice.pagoMsjChofer(datamsj)
                    //    .then(function (data) { })
                        return;
                    }
                    dataservice.payment.purchate = data.data.response;
                    console.log(" -----**** data.response: " + JSON.stringify(data.data.response));
                    
                    if(mp.guardarme){
                        //esta chequedo el guardar tajeta => guardo;
                        if(!mp.ExisteCustom){
                            dataservice.crearCustomer(mp.tarjeta)
                            .then(function (crearcustom) {
                                console.log("  respuesta: " + JSON.stringify(crearcustom));
                                dataservice.addcard(mp.tarjeta)
                                .then(function (retornoadddcard) {
                                    console.log("  respuesta: " + JSON.stringify(retornoadddcard));
                                    return;
                                })                
                            })
                        }
                        else{
                            dataservice.addcard(mp.tarjeta)
                            .then(function (retornoadddcard) {
                                console.log("  respuesta: " + JSON.stringify(retornoadddcard));
                                return;
                          })                             
                        }                                
                    }

                    $rootScope.vista = "pago";
                    $location.url('/home/pagaviaje');
                    var datamsj = {
                        idChofer: vm.idChofer,
                        payment: data.data.response
                    }
                    //console.log("  datamsj: " + JSON.stringify(datamsj));
                    dataservice.pagoMsjChofer(datamsj)
                    .then(function (data) {
                        console.log(" hizo ");
                        return;
                    })
                })
                .catch(error);
        }

    }
	
	
	
	
	

})();
